## *Kuraishia capsulata* CBS 1993

### Source

* **Data source**: [ENA](https://www.ebi.ac.uk/ena/browser/home)
* **BioProject**: [PRJEB4427](https://www.ebi.ac.uk/ena/browser/view/PRJEB4427)
* **Assembly accession**: [GCA_000576695.1](https://www.ebi.ac.uk/ena/browser/view/GCA_000576695.1)
* **Original submitter**: Genoscope CEA

### Assembly overview

* **Assembly level**: Scaffold
* **Assembly name**: AUH_PRJEB4427_v1
* **Assembly length**: 11,371,246
* **#Scaffolds**: 7
* **Mitochondiral**: No
* **N50 (L50)**: 1,678,882 (3)

### Annotation overview

* **Original annotator**: Genolevures Consortium
* **CDS count**: 6029
* **Pseudogene count**: 0
* **tRNA count**: 0
* **rRNA count**: 0
* **Mobile element count**: 0
