# Change log

This document summerizes the cumulative changes in genome annoations
since the initial annotation retieved from the EBI.

## v1.1 (2021-04-28)

## Edited

* Build the locus hierarchy.

## Fixed

* Coding gene KUCA_T00002237001 turned into pseudo (interupted by scffold boundary).
* Coding gene KUCA_T00005490001 turned into pseudo (interupted by scffold boundary).

## v1.0 (2021-04-28)

### Added

* The 7 annotated scaffolds of Kuraishia capsulata CBS 1993 (source EBI, [GCA_000576695.1](https://www.ebi.ac.uk/ena/browser/view/GCA_000576695.1)).
